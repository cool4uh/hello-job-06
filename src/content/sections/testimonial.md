---
enable: true
title: "Hello Job이 여는 성장의 문"
description: "가장 핫한 개발자 채용 정보와 기업과의 연결을 지금 경험하세요!"
# Testimonials
testimonials:
  - name: "쇼핑몰 F/E"
    designation: "쇼핑몰 F/E"
    avatar: "/images/people-07.png"
    content: "내가 일을 더 잘하기 위해서 이미 잘하고 있지만 더욱 개발할 부분과, 보완해 나가야 할 부분을 알게 되고 구체적인 강점 개발 팁을 얻어가서 좋습니다."

  - name: "스타트업 백엔드"
    designation: "스타트업 백엔드"
    avatar: "/images/people-08.png"
    content: "뛰어난 개발자들의 인사이트 있는 조언을 도움받아 드디어 원하는 스타트업에 이직을 했습니다."

  - name: "게임기획자"
    designation: "게임기획자"
    avatar: "/images/people-03.png"
    content: "게임의 세계관부터 게임캐릭터의 특성 및 이미지 생성까지 헬로잡의 도움으로 게임을 개발하여 새롭게 런칭하였습니다. 전체적인 프로젝트 기간도 엄청나게 단축되었네요."

  - name: "웹에이전시 디자이너"
    designation: "디자이너"
    avatar: "/images/people-06.png"
    content: "디자이너로서 기존에 없는 이미지를 만들기 위해 여러 가지 이미지를 검색하고 해석하고 다시 작성을 하는 시간이 어려웠고, 작성한 이미지의 중복성을 확인했었는데 이제는 헬로잡 선배님의 조언으로 고퀄 디자인이 가능해졌습니다."
---
