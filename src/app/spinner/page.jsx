import Spinner from '@/shortcodes/Spinner'

export default function Page() {
  return (
    <div> 
      <Spinner />
    </div>
  )
}