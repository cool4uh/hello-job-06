import { useState, useEffect } from "react";
import ImageFallback from "@/components/ImageFallback";
import { getListPage } from "@/lib/contentParser";
import { markdownify } from "@/lib/utils/textConverter";
import CallToAction from "@/partials/CallToAction";
import SeoMeta from "@/partials/SeoMeta";
import Testimonials from "@/partials/Testimonials";
import BannerSub1 from "@/partials/Banner1";
import BannerSub2 from "@/partials/Banner2";
import MovingBanner from "@/partials/MovingBanner";

import { FaCheck } from "react-icons/fa/index.js";
import { Feature, Button } from "types";
import { motion } from "framer-motion";
import { fadeIn, staggerContainer } from "@/utils/motion";
import styles from "./styles";
import { TitleText, TypingText } from "@/shortcodes/CustomTexts";

const Home = () => {
  // const [scrolled, setScrolled] = useState(false);

  // useEffect(() => {
  //   const handleScroll = () => {
  //     const scrollPosition = window.scrollY;
  //     const shouldAddClass = scrollPosition > 100; // 스크롤 위치가 100보다 큰 경우 "scrolled" 클래스를 추가합니다.

  //     setScrolled(shouldAddClass);
  //   };

  //   window.addEventListener('scroll', handleScroll);

  //   return () => {
  //     window.removeEventListener('scroll', handleScroll);
  //   };
  // }, []);

  const homepage = getListPage("_index.md");
  const testimonial = getListPage("sections/testimonial.md");
  const callToAction = getListPage("sections/call-to-action.md");
  const { frontmatter } = homepage;
  const {
    banner,
    features,
  }: {
    banner: { title: string; image: string; content?: string; button?: Button };
    features: Feature[];
  } = frontmatter;

  // console.log(banner.button?.enable)

  const bannerTitle = JSON.stringify(markdownify(banner.title).__html).replace(
    /\"/gi,
    ""
  );
  const bannerContent = JSON.stringify(
    markdownify(banner.content ?? "").__html
  ).replace(/\"/gi, "");

  return (
    <>
      <SeoMeta />
      <section className="section bg-gradient-72  from-custom-blue from-0% via-custom-purple via-59.3% to-custom-green to-99.5%">
        {/* background: bg-gradient-to-r linear-gradient(71.63deg, #4545F4 0%, #A74DF5 59.3%, #6EEDBD 99.5%, #6EEDBD 99.5%); */}

        {/* <section className="section pt-14 "> */}
        <div className="container  p-10">
          {/* <div className="row ">
            {banner.image && (
              <div className="col-12 border-black">
                <ImageFallback
                  src={banner.image}
                  width="1272"
                  height="403"
                  alt="banner image"
                  priority
                />
              </div>
            )}
          </div> */}
          {/* 배너 시작 */}
          <div className="flex py-[242px] h-[827px]">
            {/* <div className="border-gold  col-8 absolute  w-3/5 px-14 text-left top-1/4 left-1/2 -translate-x-1/2 -translate-y-1/2"> */}
            <div className="col-7">
              <div className=" md:text-[50px] lg:text-[60px] text-[24px] mb-5  text-white">
                {bannerTitle}
              </div>
              {/* <div className="text-white">{bannerContent}</div> */}
              {banner.button?.enable && (
                <a
                  className="border border-white text-white btn btn-outline-secondary hover:bg-slate-600 hover:text-slate-100"
                  href={banner.button.link}
                >
                  {banner.button.label}
                </a>
              )}
            </div>
            <div className="col-5 h-[827px] text-white text-4xl invisible">
              Grid-5
            </div>
          </div>
          {/* 배너 끝 */}
        </div>
      </section>

      <div className=" overflow-hidden">
        <BannerSub1 />
      </div>
      <div className=" overflow-hidden">
        <BannerSub2 />
      </div>
      {/* 
      <div className=" overflow-hidden">
        <MovingBanner />
      </div> */}
      {/* 
      {features.map((feature, index: number) => (
        <section
          key={index}
          className={`section-sm ${index % 2 === 0 && "bg-gradient"}`}
        >
          <div className="container">
            <div className="row items-center justify-between">
              <div
                className={`mb:md-0 mb-6 md:col-5 ${
                  index % 2 !== 0 && "md:order-2"
                }`}
              >
                <ImageFallback
                  src={feature.image}
                  height={480}
                  width={520}
                  alt={feature.title}
                />
              </div>
              <div
                className={`md:col-7 lg:col-6 ${
                  index % 2 !== 0 && "md:order-1"
                }`}
              >
                <h2
                  className="mb-4"
                  dangerouslySetInnerHTML={markdownify(feature.title)}
                />
                <p
                  className="mb-8 text-lg"
                  dangerouslySetInnerHTML={markdownify(feature.content)}
                />
                <ul>
                  {feature.bulletpoints.map((bullet: string) => (
                    <li className="relative mb-4 pl-6" key={bullet}>
                      <FaCheck className={"absolute left-0 top-1.5"} />
                      <span dangerouslySetInnerHTML={markdownify(bullet)} />
                    </li>
                  ))}
                </ul>
                {feature.button.enable && (
                  <a
                    className="btn btn-primary mt-5"
                    href={feature.button.link}
                  >
                    {feature.button.label}
                  </a>
                )}
              </div>
            </div>
          </div>
        </section>
      ))} */}

      <Testimonials data={testimonial} />
      <CallToAction data={callToAction} />
    </>
  );
};

export default Home;
