import { NextResponse } from "next/server";
import dbConnect from "@/lib/dbConnect";
import Forum from "@/models/Forums";

export const GET = async (request) => {
  const url = new URL(request.url);

  const username = url.searchParams.get("username");

  try {
    await dbConnect();

    // const forums = await Forum.find(username && { username });
    const forums = await Forum.find({ });

    return new NextResponse(JSON.stringify(forums), { status: 200 });
  } catch (err) {
    return new NextResponse("Database Error", { status: 500 });
  }
};

export const POST = async (request) => {
  const body = await request.json();

  const newForum = new Forum(body);

  try {
    await dbConnect();

    await newForum.save();

    return new NextResponse("Forum has been created", { status: 201 });
  } catch (err) {
    return new NextResponse("Database Error", { status: 500 });
  }
};
