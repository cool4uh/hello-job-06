/**
 * @swagger
 * /api/tags:
 *   get:
 *     description: Returns Tags data
 *     responses:
 *       200:
 *         description: Tags data
 */
import { NextResponse } from "next/server";
import dbConnect from "@/lib/dbConnect";
import Tag from "@/models/Tags";

export const GET = async (request) => {
  const url = new URL(request.url);

  // const username = url.searchParams.get("username");

  try {
    await dbConnect();

    const tags = await Tag.find({ });

    return new NextResponse(JSON.stringify(tags), { status: 200 });
  } catch (err) {
    return new NextResponse("Database Error", { status: 500 });
  }
};

export const POST = async (request) => {
  const body = await request.json();

  const newTag = new Tag(body);

  try {
    await dbConnect();

    await newTag.save();

    return new NextResponse("Tag has been created", { status: 201 });
  } catch (err) {
    return new NextResponse("Database Error", { status: 500 });
  }
};
