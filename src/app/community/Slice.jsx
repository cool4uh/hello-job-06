import React from "react";
import { CiRead } from "react-icons/ci/index";
import { BiCommentDetail } from "react-icons/bi/index";




export default function Slice()  {
  return (
    <div className="border-t-[3px] py-4">
      <div className="flex flex-col">
        <div className="flex shrink-0 items-center gap-x-1">
          <div>뿡삥이</div>
          <div>.</div>
          <div>번개</div>
          <div>10</div>
          <div>.</div>
          <div>약 4시간전</div>
        </div>
        <div className="line-clamp-1 w-fit truncate whitespace-normal break-all text-sm font-semibold leading-6 text-gray-900 hover:text-blue-500 dark:text-gray-100 dark:hover:text-blue-200 sm:text-base">
          사당/토) 인공지능 퀀트 스터디 모집
        </div>
        <div className="flex">
          <div className="flex flex-1 items-center gap-x-3">
            <div className="line-clamp-1 text-xs font-normal leading-5 text-gray-600 hover:text-blue-500 dark:text-gray-400 dark:hover:text-blue-200 sm:text-sm">
              #사당
            </div>
            <div className="line-clamp-1 text-xs font-normal leading-5 text-gray-600 hover:text-blue-500 dark:text-gray-400 dark:hover:text-blue-200 sm:text-sm">
              #퀀트
            </div>
            <div className="line-clamp-1 text-xs font-normal leading-5 text-gray-600 hover:text-blue-500 dark:text-gray-400 dark:hover:text-blue-200 sm:text-sm">
              #인공지능
            </div>
          </div>
          <div className="flex items-center gap-x-2 text-gray-700 dark:text-gray-300">
            <div className="inline-flex items-center space-x-0.5 text-xs sm:text-sm">
              {/* <svg></svg> */}
              <CiRead size="26" color="#3c3838" />
              <span>251</span>
            </div>
            <div className="inline-flex items-center space-x-0.5 text-xs sm:text-sm">
              {/* <svg></svg> */}
              <BiCommentDetail size="26" color="#3c3838" />

              <span>5</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
