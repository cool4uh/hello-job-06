"use client";

import config from "@/config/config.json";
import { useTheme } from "next-themes";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";

const Logo = ({ src, isScrolled }: { src?: string; isScrolled?: boolean }) => {
  // destructuring items from config object
  const {
    logo,
    logo_darkmode,
    logo_width,
    logo_height,
    logo_text,
    title,
  }: {
    logo: string;
    logo_darkmode: string;
    logo_width: any;
    logo_height: any;
    logo_text: string;
    title: string;
  } = config.site;

  const { theme, resolvedTheme } = useTheme();
  const [mounted, setMounted] = useState(false);
  const [scrolled, setScrolled] = useState(false);

  useEffect(() => setMounted(true), []);

  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY;
      const shouldAddClass = scrollPosition > 500; // 스크롤 위치가 100보다 큰 경우 "scrolled" 클래스를 추가합니다.

      setScrolled(shouldAddClass);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const resolvedLogo =
    mounted && (theme === "dark" || resolvedTheme === "dark")
      ? logo_darkmode
      : logo;
  const logoPath = src ? src : resolvedLogo;

  return (
    <div>
      <Link href="/" className="navbar-brand inline-block">
        {logoPath ? (
          <div className="flex flex-row">
            <Image
              width={logo_width.replace("px", "") * 3}
              height={logo_height.replace("px", "") * 3}
              src={logoPath}
              alt={title}
              priority
              style={{
                height: logo_height.replace("px", "") + "px",
                width: logo_width.replace("px", "") + "px",
              }}
            />
            <div className={scrolled ? "ml-2 text-dark" : "ml-2 text-white"}>
              {logo_text}
            </div>
          </div>
        ) : logo_text ? (
          logo_text
        ) : (
          title
        )}
      </Link>
    </div>
  );
};

export default Logo;
