"use client";

import Image from "next/image";

const BannerSub2 = () => (
  <section className="section relative z-10">
    <div className="container">
      <div className="row bg-slate-200">
        <div className="col-2"></div>

        <div className="col-4 space-y-4 my-[171px]">
          {/* <div className="flex-col"> */}
          <div>
            <h1 className="text-[#5247EC] text-[48px]">헬로잡에서는?</h1>
          </div>
          <div className="">
            <div className="text-xl">
              <p className="text-[#5B5B5B]">
                개발자들의 취업 매칭을 AI 기술로 혁신합니다.
              </p>
            </div>
            <div className="text-xl">
              <p className="text-[#5B5B5B]">
                지금, 미래의 코더로 거듭나보세요!
              </p>
            </div>
          </div>
          {/* </div> */}
        </div>
        <div className="col-4 space-y-4 my-[171px] text-center">
          {/* <div className="flex-col"> */}
          <div className="text-[18px] bg-white py-[18px] px-[58px] rounded-2xl">
            <p className="text-[#5B5B5B]">개발자들이 모이는 취업의 성지</p>
          </div>
          <div className="text-[18px] bg-white py-[18px] px-[58px] rounded-2xl">
            <p className="text-[#5B5B5B]">
              신입 개발자를 위한 최고의 취업 플랫폼
            </p>
          </div>
          <div className="text-[18px] bg-white py-[18px] px-[58px] rounded-2xl">
            <p className="text-[#5B5B5B]">최신 개발자 채용정보 한 곳에</p>
          </div>
          <div className="text-[18px] bg-white py-[18px] px-[58px] rounded-2xl">
            <p className="text-[#5B5B5B]">개발자들의 인생을 바꿀 기회</p>
          </div>
          {/* </div> */}
        </div>
        <div className="col-2"></div>
      </div>
    </div>
  </section>
);

export default BannerSub2;
