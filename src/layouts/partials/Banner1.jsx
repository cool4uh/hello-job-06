"use client";

import Image from "next/image";

const BannerSub1 = () => (
  <section className="section">
    <div className="container ">
      <div className="row px-[128px] py-[0px] ">
        <div className="col-3 flex justify-end">
          <Image
            src="/images/bulb.png"
            alt="get-started"
            width={87}
            height={87}
            className="w-[87px] h-[87px]"
          />
        </div>
        <div className="col-9 flex ">
          <p className="mt-[12px] font-normal  sm:text-3xl tracking-wide text-black">
            &nbsp; &nbsp; &nbsp; &nbsp; 스타트업의 경우에는 이직이 활발하기
            때문에, &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; 짧은 기간동안의 이력을 잘 정리하는게 정말 중요하죠.
          </p>
        </div>
      </div>
    </div>
  </section>
);

export default BannerSub1;
