"use client";

import Logo from "@/components/Logo";
import Social from "@/components/Social";
import config from "@/config/config.json";
import menu from "@/config/menu.json";
import social from "@/config/social.json";
import { markdownify } from "@/lib/utils/textConverter";
import Link from "next/link";

const Footer = () => {
  const { copyright } = config.params;

  return (
    <footer className="section">
      <div className="container">
        <div className="row items-center">
          <div className="mb-8 text-center lg:col-3 lg:mb-0 lg:text-left">
            <Logo />
          </div>
        </div>
        <div className="flex gap-3 text-slate-800 lg:col-6 lg:mb-0">
          <Link href='/terms'>서비스이용약관</Link>
          <span>|</span>
          <Link href='/privacy-policy'>개인정보취급방침</Link>
          {/* <ul className="flex justify-evenly">
            {menu.footer.map((menu) => (
              <li className="m-1 p-1 inline-block" key={menu.name}>
                <Link href={menu.url}>{menu.name}</Link>
              </li>
            ))}
          </ul> */}
        </div>
        {/* <div className="mb-8 text-center lg:col-3 lg:mb-0 lg:mt-0 lg:text-right">
            <Social source={social} className="social-icons" />
          </div> */}
      </div>
      <div className="">
        <div className="container  text-slate-800 dark:text-darkmode-light">
          <p dangerouslySetInnerHTML={markdownify(copyright)} />
        </div>
      </div>
    </footer>
  );
};

export default Footer;
