import ImageFallback from "@/components/ImageFallback";
import { markdownify } from "@/lib/utils/textConverter";
import { Call_to_action } from "types";

interface PageData {
  notFound?: boolean;
  content?: string;
  frontmatter: Call_to_action;
}

const CallToAction = ({ data }: { data: PageData }) => {
  return (
    <>
      {data.frontmatter.enable && (
        <section className=" bg-gradient-180  from-custom-violet from-0% to-custom-violet2 to-99.5% ">
          {/*  */}
          <div className="container">
            {/* <div className="rounded-xl bg-theme-light px-4 py-16 dark:bg-darkmode-theme-light xl:p-20"> */}
            <div className="rounded-xl  px-4 py-16 dark:bg-darkmode-theme-light xl:p-20">
              <div className="row items-center justify-between">
                {/* <div className="mb-10 md:col-5 lg:col-4 md:order-2 md:mb-0">
                  <ImageFallback
                    className="w-full"
                    src={data.frontmatter.image}
                    width={392}
                    height={390}
                    alt="cta-image"
                  />
                </div> */}
                <div className="md:col-6  invisible">Grid </div>
                <div className=" md:col-6 md:order-1 ">
                  <h2
                    dangerouslySetInnerHTML={markdownify(
                      data.frontmatter.title
                    )}
                    className="p-[10px] text-white text-[32px]"
                  />
                  {/* <p
                    dangerouslySetInnerHTML={markdownify(
                      data.frontmatter.description
                    )}
                    className="mb-6"
                  /> */}
                  {data.frontmatter.button.enable && (
                    <a
                      className="btn btn-primary"
                      href={data.frontmatter.button.link}
                    >
                      {data.frontmatter.button.label}
                    </a>
                  )}
                </div>
              </div>
            </div>
          </div>
        </section>
      )}
    </>
  );
};

export default CallToAction;
