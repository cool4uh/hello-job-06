"use client";

import { motion } from "framer-motion";
// import { newFeatures } from '../constants';
import styles from "./styles";
import { TitleText, TypingText } from "@/shortcodes/CustomTexts";
import { planetVariants, fadeIn, staggerContainer } from "@/utils/motion";
// import { NewFeatures } from "./NewFeatures";
import Image from "next/image";

const newFeatures = [
  {
    imgUrl: "/images/vrpano.svg",
    title: "신입 개발자를 위한 최고의 취업 플랫폼",
    subtitle: "헬로잡에서 당신의 꿈을 이뤄보세요",
  },
  {
    imgUrl: "/images/headset.svg",
    title: "개발자들이 모이는 취업의 성지",
    subtitle: "헬로잡이 함께하는 개발자들의 성장 이야기, 지금 시작해보세요!",
  },
];

const NewFeatures = ({ imgUrl, title, subtitle }) => (
  <div className="flex-1 flex flex-col sm:max-w-[250px] min-w-[210px]">
    <div
      className={`${styles.flexCenter} w-[70px] h-[70px] rounded-[24px] bg-[#323F5D]`}
    >
      <Image
        src={imgUrl}
        alt="icon"
        width={200}
        height={100}
        className="w-1/2 h-1/2 object-contain"
      />

      {/* <img src={imgUrl} alt="icon" className="w-1/2 h-1/2 object-contain" /> */}
    </div>
    <h1 className="mt-[26px] font-bold text-[24px] leading-[30.24px] text-[#4545F4]">
      {title}
    </h1>
    <p className="flex-1 mt-[16px] font-normal text-[18px] text-[#5B5B5B] leading-[32.4px]">
      {subtitle}
    </p>
  </div>
);

const MovingBanner = () => (
  <section className={`section ${styles.paddings} relative z-10`}>
    <div className="container">
      <div className="row ">
        <motion.div
          variants={staggerContainer}
          initial="hidden"
          whileInView="show"
          viewport={{ once: false, amount: 0.25 }}
          className={`${styles.innerWidth} mx-auto flex lg:flex-row flex-col gap-8`}
        >
          <motion.div
            variants={fadeIn("right", "tween", 0.2, 1)}
            className="flex-[0.95] flex justify-center flex-col"
          >
            <TitleText title={<>헬로잡에서는?</>} textStyles="text-[#4545F4]" />
            <TypingText
              title="| 개발자들의 '취업 매칭'을 AI 기술로 혁신합니다.
지금, 미래의 코더로 거듭나보세요!"
            />
            <div className="mt-[48px] flex flex-wrap justify-between gap-[24px]">
              {newFeatures.map((feature) => (
                <NewFeatures key={feature.title} {...feature} />
              ))}
            </div>
          </motion.div>

          <motion.div
            variants={planetVariants("right")}
            className={`flex-1 ${styles.flexCenter}`}
          >
            <Image
              src="/images/sphere.png"
              alt="get-started"
              width={200}
              height={200}
              className="w-[100%] h-[100%] object-contain"
            />
            {/* <img
              src="/images/sphere.png"
              alt="get-started"
              className="w-[100%] h-[100%] object-contain"
            /> */}
          </motion.div>
          <motion.div
            variants={fadeIn("left", "tween", 0.2, 1)}
            className="flex-[0.5] lg:max-w-[370px] flex justify-end flex-col gradient-05 sm:p-8 p-4 rounded-[32px] border-[1px] border-[#6A6A6A] relative"
          >
            <div className="feedback-gradient" />
            <div>
              <h4 className="font-bold sm:text-[32px] text-[26px] sm:leading-[40.32px] leading-[36.32px] text-[#4545F4]">
                Olive
              </h4>
              <p className="mt-[8px] font-normal sm:text-[18px] text-[12px] sm:leading-[22.68px] leading-[16.68px] text-slate-600">
                Founder HelloJob
              </p>
            </div>

            <p className="mt-[24px] font-normal sm:text-[24px] text-[18px] sm:leading-[45.6px] leading-[39.6px] text-slate-500">
              “포트폴리오 경력기술서라는게 사실 참 어려운거같아요. 단순히 내가
              해왔던 일들을 나열하는것이 아니라, 그 중에서 필요없는 부분은
              버리고, 강조해야할 부분을 찾아서 어필해야 한다는 점이 참 쉽지
              않아요. 그리고 스타트업의 경우에는 이직이 활발하기 때문에, 이 짧은
              기간동안의 이력을 잘 정리하는게 정말 중요하죠.”
            </p>
          </motion.div>
        </motion.div>
      </div>
    </div>
  </section>
);

export default MovingBanner;
