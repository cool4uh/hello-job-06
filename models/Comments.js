import mongoose from "mongoose";

/* 커뮤니티 게시글  */
const CommentSchema = new mongoose.Schema({
  comments: {
    type: String,
    required: [true, "Please provide a name for this post."],
    maxlength: [500, "Name cannot be more than 500 characters"],
  },
  postId: {
    type: mongoose.Schema.Types.ObjectId, // Post 스키마의 ObjectId 타입으로 정의
    ref: "Post", // 참조할 모델의 이름
  },
  createdBy: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  totalViews: {
    type: Number,
    default: 0,
  },
});

export default mongoose.models.Comment ||
  mongoose.model("Comment", CommentSchema);
